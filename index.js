// Carregar o modulo para noje
const AWS = require('aws-sdk');

// Registrar o ACCESS_KEY e SECRET_KEY
const credentials = {
    id: '_ACCESS_KEY_AWS_',
    secret: '_SECRET_KEY_AWS_'
}

// Setar a regiao
AWS.config.update({ region: 'us-east-1' });
// Chamar as credenciais
AWS.config.accessKeyId = credentials.id
AWS.config.secretAccessKey = credentials.secret

// Criar os parametros públicos
let params = {
  Message: 'Olá Mundo, meu primeiro SMS for JavaScript', /* obrigatorio */
  PhoneNumber: '+5561999999999'
};

// Criacao da promise e do objeto a ser instanciado
function sendSMS(params) {
    var publishTextPromise = new AWS.SNS().publish(params).promise();
    // Retorna Menssagem ou erro
    publishTextPromise.then(function (data) {
        console.log("MenssagemID é " + data.MessageId);
    }).catch(function (err) {
        console.error(err, err.stack);
    });
}

// Envia o sms
sendSMS(params);

# sms-javascript

![Language Version](https://img.shields.io/node/v/aws-sdk?style=plastic)

Envio de SMS utilizando javascript e AWS SNS

## Instruções

Criar um usuário/grupo no IAM com permissao apenas no SNSFULLACCESS

Para iniciar o projeto foi feito:

```
$ npm init
```

```
$ npm i aws-sdk
```

Crie um arquivo index.js

Crie a constante no index.js para armazenar as credenciais

```
const credentials = {
    id: '_ACCESS_KEY_AWS_',
    secret: '_SECRET_KEY_AWS_'
}
```

Acesse a documentacao para pegar o codigo meio que pronto
    https://docs.aws.amazon.com/pt_br/sdk-for-javascript/v2/developer-guide/sns-examples-publishing-messages.html

Verificar a regiao a ser usada: no caso da virgina use us-east-1 ou olhe no quadro abaixo
    https://docs.aws.amazon.com/pt_br/AWSEC2/latest/UserGuide/using-regions-availability-zones.html 


## Para baixar o projeto pronto e executar

```
$ git clone https://gitlab.com/nettask/sms-javascript.git
```

```
$ npm install
```

Alterem em index.js as constantes

Infome o Texto de envio na variavel: Message
Infome o Telefone de envio na vaiavel: FoneNumber no formato +5561999999999

Execute o projeto com a linha abaixo

```
$ node index.js
```
